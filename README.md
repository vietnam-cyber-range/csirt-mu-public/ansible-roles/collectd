# Ansible role - Collectd

This role serves for automatic installation of statiscics collection daemon collectd.

## Requirements

This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

```yml
become: yes
```

## Role parameters

* `collectd_global_settings` (optional) - Object of `key: value` global settings. For more information about global settings see [collectd.conf(5)](https://collectd.org/documentation/manpages/collectd.conf.5.shtml#global_options)
* `collectd_load_plugins` (optional) - List of plugins to load. For more information about plugins see [collectd.conf(5)](https://collectd.org/documentation/manpages/collectd.conf.5.shtml#plugin_options)
* `collectd_plugins_configuration` (optional) - List of sections for specific plugin configuration. For more information about section format see [apache_encode_macro.j2](templates/apache_encode_macro.j2). For more information about specific plugin configuration see [collectd.conf(5)](https://collectd.org/documentation/manpages/collectd.conf.5.shtml#plugin_options)

## Example

Example of simple Collectd installation.

```yml
roles:
    - role: collectd
      become: yes
```

Example of Collectd installation with advanced configuration.

```yml
roles:
    - role: collectd
      collectd_global_settings:
          FQDNLookup: false
      collectd_load_plugins:
          - df
          - threshold
      collectd_plugins_configuration:
          - name: Plugin
            param: df
            content:
                - options:
                      - ReportInodes: true
                      - ValuesPercentage: true
          - name: Plugin
            param: threshold
            content:
                - sections:
                      - name: Plugin
                        param: df
                        content:
                            - options:
                                  Instance: root
                            - sections:
                                  - name: Type
                                    param: percent_bytes
                                    content:
                                        - options:
                                              - Persist: true
                                                PersistOK: true
                                                Instance: used
                                                WarningMax: 70
                                                FailureMax: 80
      become: yes
```